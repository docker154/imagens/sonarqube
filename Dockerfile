FROM openjdk:8-jdk-alpine

COPY sonar/ /sonar
ENV PATH="${PATH}:/sonar/bin" 